#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;


class Clock {
  public:
    Clock(int hour, int minute);
    void tick_tock();
    void print() const;
    void count_time();
    int get_minutes();
    int time_difference(Clock time2);

  private:
    int hours_;
    int minutes_;
    int new_minutes_;
};

int Clock::time_difference(Clock time2){
    int time_dif;
    time_dif = (get_minutes()- time2.get_minutes());
    if (time_dif < 0){
        time_dif = abs(time_dif);
    }

    else if (time_dif > 720){
        time_dif = 1440 - time_dif;
    }

    return time_dif;

}

int main() {

    Clock time1(8, 30);
    Clock time2(17, 21);

    time1.count_time();
    time2.count_time();

    cout << time1.time_difference(time2) <<endl;
    time1.tick_tock();

}


Clock::Clock(int hour, int minute):
    hours_(hour), minutes_(minute) {
}

void Clock::count_time() {
    minutes_ = minutes_ + hours_ *60;
    hours_ = 0;


}

int Clock::get_minutes() {
    return minutes_;
}




void Clock::tick_tock() {
    ++minutes_;
    if ( minutes_ >= 60 ) {
        minutes_ = 0;
        ++hours_;
    }

    if ( hours_ >= 24 ) {
        hours_ = 0;
    }
}


 /* void Clock::print() const {
    cout << setw(2) << setfill('0') << hours_
       << "."
         << setw(2) << minutes_
         << endl;
} */
